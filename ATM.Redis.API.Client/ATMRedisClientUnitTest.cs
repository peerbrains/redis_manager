﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace ATM.Redis.API.Client
{
    public class RedisKeyValuedPair
    {
        public string key { get; set; }
        public string value { get; set; }
    }
    [TestClass]
    public class ATMRedisClientUnitTest
    {
        readonly ConfigRequest colValues = null;
        readonly RedisKeyValuedPair keyValuedPair = null;
        readonly RedisKeyValuedPair addKeyValuedEmpty = null;
        readonly SysProperty addSysProperty = null;
        readonly string urlEndPoint = ConfigurationManager.AppSettings["ATMSettingsURL"].ToString();
        public ATMRedisClientUnitTest()
        {
            colValues = new ConfigRequest { Data = "Unit Testing for to ATM.Redis.API", ListFilter = new List<Filters> { new Filters { Title = "ATM.Redis.API", Filter = "KeyValue", Field = " GetProperty" } } };
            keyValuedPair = new RedisKeyValuedPair { key = "", value = "Unit Testing for to ATM.Redis.APIClient" };
            addKeyValuedEmpty = new RedisKeyValuedPair {key="",value="" };
            addSysProperty = new SysProperty { Host="PeerBrains", PropId="db_Name3", PropValue="TestData3", SysCaching=true, HostPropId="PeerBrains:db_Name3" };
        }

        [TestMethod]
        public void GetProperty_WhenKeyIsValid()
        {
            string keyValueForRedis = "GetProperty/Peerbrains:List_filter";
            string apiUrl = urlEndPoint + keyValueForRedis;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;

                    var responseData = JsonConvert.DeserializeObject<RedisKeyValuedPair>(data);

                }
            }
        }

        [TestMethod]
        public void GetProperty_WhenKeyIsNull()
        {
            string keyValueForRedis = "GetProperty/";

            string apiUrl = urlEndPoint + keyValueForRedis;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;

                    var responseData = JsonConvert.DeserializeObject<RedisKeyValuedPair>(data);


                }
            }
        }

        [TestMethod]
        public void GetProperty_WhenKeyIsValid_WhereDataIsEmpty()
        {
            string keyValueForRedis = "GetProperty/Peerbrains:db_Name";
            string apiUrl = urlEndPoint + keyValueForRedis;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;

                    var responseData = JsonConvert.DeserializeObject<RedisKeyValuedPair>(data);


                }
            }
        }
        
       
        [TestMethod]
        public void GetCacheAll_GetByDomain_WhenKeyIsValid()
        {
            string keyValueForRedis = "GetCacheAll/PeerBrains";

            string apiUrl = urlEndPoint + keyValueForRedis;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;

                    var responseData = JsonConvert.DeserializeObject<List<RedisKeyValuedPair>>(data);

                }
            }
        }
        [TestMethod]
        public void AddProperty_WhenKeyIsValid()
        {
            HttpStatusCode result;
            string keyForRedis = "AddProperty/PeerBrains/List_Filter";
            string apiUrl = urlEndPoint + keyForRedis;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(JsonConvert.SerializeObject(keyValuedPair), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }


        [TestMethod]
        public void AddProperty_WithNoValue_WhenKeyIsValid()
        {
            HttpStatusCode result;
            string keyForRedis = "AddProperty/PeerBrains/db_Name";
            
            string apiUrl = urlEndPoint + keyForRedis;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(JsonConvert.SerializeObject(addKeyValuedEmpty), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;

            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }

        
        [TestMethod]
        public void DeleteProperty_WhenDataIsValid()
        {
            HttpStatusCode result;
            string keyForRedis = "DeleteProperty/PeerBrains/List_Filter";
            //string valueForRedis = "ATM Setting API";
            string apiUrl = urlEndPoint + keyForRedis;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(JsonConvert.SerializeObject(keyValuedPair), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
    }
}

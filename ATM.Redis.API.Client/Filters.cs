﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Redis.API.Client
{
    public class Filters
    {
        public string Title { get; set; }
        public string Filter { get; set; }
        public string Field { get; set; }

    }

    public class ConfigRequest
    {
        public string Data { get; set; }

        public List<Filters> ListFilter { get; set; }
    }

    public class SysProperty
    {

        public string PropId { get; set; }
        public string HostPropId { get; set; }
        public string PropValue { get; set; }
        public string Descripition { get; set; }
        public string PropGroup { get; set; }
        public string Host { get; set; }
        public int UserPrefID { get; set; }
        public bool SysCaching { get; set; }
        public DateTime DateCreated { get; set; }
        public long CreatedBy { get; set; }
        public DateTime DateUpdated { get; set; }
        public long UpdatedBy { get; set; }
    }
}

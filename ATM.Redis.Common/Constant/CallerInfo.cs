﻿using System;
using System.Runtime.CompilerServices;

namespace ATM.Redis.Common.Constant
{
    public sealed class CallerInfo : Attribute
    {
        public static string GetCurrentMethodName([CallerMemberName]string methodName = "")
        {
            return methodName;
        }
    }
}

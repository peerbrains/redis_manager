# Introduction 
ATM.Redis.API - Connector to Redis database. .NET Core microservices built in Core 2. The project has unit testing also swagger available for documentation purpose. 

# Getting Started
ATM.Redis.API - 
appsettings.json - The JSON file holds the Redis Server reference.
nlog.config - Logging Folder reference is available in the nlog config file.
API.Redis.Client - 
app.config - 
ATMSettingsURL - the Redis app url for client test is mentioned here.
EndPoints - 
1. GetProperpty - GET Endpoint passing KeyID and returns the value as JSON object.
2. GetPropertiesBy - GET Endpoint passing Host and returns all the key id / value pair as JSON object.
3. AddProperty - POST Endpoint passing Host and KeyID and returns the value as JSON object.


# Build and Test
V1.1 
1. - Added Delete end-point
2. - Moved the constants from Logging and error messages to the constant file.
3. - Added CalledInfo Class file to remove generically add the called method name.
4. - Changed the stackExchange connector method of calling the Redis Server.
# Contribute

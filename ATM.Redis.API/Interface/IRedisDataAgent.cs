﻿using ATM.Redis.API.Factory;
using System.Collections.Generic;

namespace ATM.Redis.API.Interface
{
    public interface IRedisDataAgent
    {
        string GetStringValue(string key);
        string GetAllStringValues(string domainName);
        List<RedisKeyValuePair> GetAllKeyValues(string domainName);
        bool SetStringValue(string key, string value);
        bool DeleteStringValue(string key);
        bool Add<T>(string key, T value) where T : class;
    }
}

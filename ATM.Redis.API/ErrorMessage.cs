﻿namespace ATM.Redis.API
{
    public class ErrorMessage
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}

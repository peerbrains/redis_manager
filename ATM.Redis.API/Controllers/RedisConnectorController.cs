﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ATM.Redis.API.Factory;
using Microsoft.AspNetCore.Cors;
using System.Reflection;
using ATM.Redis.Common.Constant;
using StackExchange.Redis;
using static ATM.Redis.Common.Constant.Constant;

namespace ATM.Redis.API.Controllers
{

    /// <summary>
    /// Redis API to handle CRUD operation for caching.
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class RedisController : Controller
    {
        #region Private Variables
        private readonly ILogger _logger;
        private readonly string _redisServer = string.Empty;
        private readonly string _redisConnectionMultiplexer = string.Empty;
        private readonly string _redisdb = string.Empty;

        #endregion

        #region Constructor        
        public RedisController(ILogger<RedisController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _redisServer = configuration["RedisServer"];
            _redisConnectionMultiplexer = configuration["RedisConnectionMultiplexer"];
        }
        #endregion

        #region API Methods

        /// <summary>
        /// Gets cache value based on provided key.
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns>Retunrs key value</returns>       
        [HttpGet]
        [Route("GetProperty/{keyName}")]
        [ProducesResponseType(typeof(RedisKeyValuePair), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public IActionResult GetProperty(string keyName)
        {
            _logger.LogInformation(String.Format(RedisAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, keyName));
            try
            {
                var dataAgent = new RedisDataAgent(_redisServer, _redisConnectionMultiplexer);
                string domainName = keyName.Split(":")[0];
                if (domainName.IndexOf('.') > 0)
                    domainName = domainName.Substring(0, domainName.IndexOf('.'));
                var keyValue = dataAgent.GetStringValue(domainName + ":" + keyName.Split(":")[1]);
                if (string.IsNullOrEmpty(keyValue))
                {
                    _logger.LogInformation(String.Format("The value of keyValue variable is :{0}", keyValue));
                }
                RedisKeyValuePair response = new RedisKeyValuePair();
                response.Key = domainName + ":" + keyName.Split(":")[1];
                response.Value = keyValue;

                _logger.LogInformation(String.Format(RedisAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, keyName));

                return new OkObjectResult(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(RedisAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Gets all key value for specified domain.
        /// </summary>
        /// <param name="domainName"></param>
        /// <returns>Retunrs key values for specified domain</returns>       
        [HttpGet]
        [Route("GetCacheAll/{domainName}")]
        [ProducesResponseType(typeof(IList<RedisKeyValuePair>), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public IActionResult GetCacheAll(string domainName)
        {
            _logger.LogInformation(String.Format(RedisAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, domainName));
            try
            {
                var dataAgent = new RedisDataAgent(_redisServer, _redisConnectionMultiplexer);
                if (domainName.IndexOf('.') > 0)
                    domainName = domainName.Substring(0, domainName.IndexOf('.'));

                var keyValue = dataAgent.GetAllKeyValues(domainName);
                if (keyValue != null && keyValue.Count > 0)
                {
                    _logger.LogInformation(String.Format("The value of keyValue variable is :{0}", keyValue));
                }
                _logger.LogInformation(String.Format(RedisAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, domainName));
                return new OkObjectResult(keyValue);

            }
            catch (Exception ex)
            {

                _logger.LogError(String.Format(RedisAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, domainName));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = "Error occurred by " + ex.Message });
            }
        }




        /// <summary>
        /// Saves Cache key.
        /// </summary>
        /// <param name="domainName"></param>
        /// <param name="propertyId"></param>
        /// <param name="propValue"></param>
        /// <returns>Retunrs key values for specified domain</returns>      
        [HttpPost]
        [Route("AddProperty/{domainName}/{propertyId}")]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public async Task<IActionResult> Post(string domainName, string propertyId, [FromBody] Object propValue)
        {
            _logger.LogInformation(String.Format(RedisAPIConstant.APICallStarted, CallerInfo.GetCurrentMethodName(), propValue));
            try
            {
                bool retVal = false;
                var keyvalue = "";
                var dataAgent = new RedisDataAgent(_redisServer, _redisConnectionMultiplexer);
                if (propValue != null)
                {
                    var usr = Newtonsoft.Json.JsonConvert.DeserializeObject<RedisKeyValuePair>(propValue.ToString());

                    if (domainName.IndexOf('.') > 0)
                        domainName = domainName.Substring(0, domainName.IndexOf('.'));
                    keyvalue = usr.Value;
                }
                retVal = dataAgent.Add(domainName + ":" + propertyId, keyvalue);

                if (retVal == true)
                    return Ok("Success");
                return Ok("Failure");
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(RedisAPIConstant.APICallError, CallerInfo.GetCurrentMethodName(), ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = "Error occurred by " + ex.Message });
            }

        }
        /// <summary>
        /// Deletes Cache Key.
        /// </summary>
        /// <param name="Host"></param>
        /// <param name="propertyId"></param>
        /// <returns>Deletes key values for specified domain/PropertyID</returns> 
        [HttpPost]
        [Route("DeleteProperty/{Host}/{propertyId}")]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public async Task<IActionResult> DeleteProperty(string Host, string propertyId)
        {
            var hostPropID = Host + ":" + propertyId;

            _logger.LogInformation(String.Format(RedisAPIConstant.APICallEnded, CallerInfo.GetCurrentMethodName(), hostPropID));
            try
            {
                bool retVal = false;
                ConnectionMultiplexer _redis = ConnectionMultiplexer.Connect(_redisConnectionMultiplexer);
                IDatabase _db = _redis.GetDatabase();
                retVal = _db.KeyDelete(hostPropID);
                _logger.LogInformation(String.Format(RedisAPIConstant.APICallEnded, CallerInfo.GetCurrentMethodName(), hostPropID));
                if (retVal == true)
                {
                    return StatusCode(200, new ErrorMessage { Status = "Sucess", Message = "Success" });
                }
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = "Data not deleted." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(RedisAPIConstant.APICallError, CallerInfo.GetCurrentMethodName(), ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }
        #endregion
    }
}

﻿namespace ATM.Redis.API.Factory
{
    public class RedisKeyValuePair
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}

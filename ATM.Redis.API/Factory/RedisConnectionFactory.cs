﻿using StackExchange.Redis;
using System;

namespace ATM.Redis.API.Factory
{
    public class RedisConnectionFactory
    {
        private readonly Lazy<ConnectionMultiplexer> Connection;

        public RedisConnectionFactory(string connectionMultiplexer)
        {
            Connection = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(connectionMultiplexer));
        }
        public ConnectionMultiplexer GetConnection() => Connection.Value;
    }
}

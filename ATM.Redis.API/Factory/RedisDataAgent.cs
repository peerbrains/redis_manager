﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;

namespace ATM.Redis.API.Factory
{
    public class RedisDataAgent
    {
        private static IDatabase _database;
        private static IServer _server;

        public RedisDataAgent(string redisServer, string redisConnectionMultiplexer)
        {
            try
            {
                RedisConnectionFactory factory = new RedisConnectionFactory(redisConnectionMultiplexer);
                var connection = factory.GetConnection();
                _database = connection.GetDatabase();
                _server = connection.GetServer(redisServer);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetStringValue(string key)
        {
            return _database.StringGet(key);
        }

        public string GetAllStringValues(string domainName)
        {
            try
            {
                string keyValuePairs = "";
                domainName = "*" + domainName + "*";
                string val = "";
                foreach (var key in _server.Keys(pattern: domainName))
                {
                    val = _database.StringGet(key);
                    keyValuePairs = key.ToString() + ":" + val + ";" + keyValuePairs;
                }
                return keyValuePairs;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public List<RedisKeyValuePair> GetAllKeyValues(string domainName)
        {
            List<RedisKeyValuePair> lstRedisKeyValue = new List<RedisKeyValuePair>();
            domainName = "*" + domainName + "*";
            string val = "";
            foreach (var key in _server.Keys(pattern: domainName))
            {
                val = _database.StringGet(key);
                lstRedisKeyValue.Add(new RedisKeyValuePair { Key = key.ToString(), Value = val });
            }
            return lstRedisKeyValue;
        }

        public bool SetStringValue(string key, string value)
        {
            try
            {
                bool retVal = false;
                retVal = _database.StringSet(key, value);
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteStringValue(string key)
        {
            try
            {
                return _database.KeyDelete(key);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add<T>(string key, T value) where T : class
        {
            try
            {
                //var serializedObject = JsonConvert.SerializeObject(value);
                var serializedObject = value.ToString();
                return _database.StringSet(key, serializedObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
